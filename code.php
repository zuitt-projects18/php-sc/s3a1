<?php

//Create a person class with three properties

class Person {
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }
//Create a printName() method
    public function printName(){
        return "Your Fullname is $this->firstName $this->middleName $this->lastName.";
    }
};

//Create two child classes for Person: Developer and Engineer

class Developer extends Person {
    public function printName(){
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
    }
};

class Engineer extends Person {
    public function printName(){
        return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
    }
};

$personOne = new Person("Senku","","Ishigami");
$developerOne = new Developer("John","Finch","Smith");
$engineerOne = new Engineer("Harold","Myers","Reese");
